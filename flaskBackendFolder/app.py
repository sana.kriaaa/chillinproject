from flask import Flask,request,render_template,redirect
from neo4j import GraphDatabase
import csv

with open("flaskBackendFolder/cred.txt")as f1:
 data=csv.reader(f1,delimiter=",")
 for row in data:
    id=row[0]
    pwd=row[1]
 f1.close()

driver=GraphDatabase.driver(uri="bolt://100.26.216.133:33998",auth=(id,pwd),encrypted=False)
session=driver.session(database="eventdb")

app=Flask(__name__)
@app.route("/graph",methods=["GET","POST"])
def creategrpah():
    if request.method=="POST":
        if request.form["submit"]=="find_graph":

         query="""
        MATCH(a:Event)
        return a.title as title ,a.date as date, a.lieu as lieu
        """
         results=session.run(query)
         graphs=[]
         for result in results:
            dc={}
            title=result["title"]
            date=result["date"]
            lieu=result["lieu"]
            dc.update({"Title":title,"Date":date, "Lieu":lieu})
            graphs.append(dc)
         print(graphs)
         return render_template("results.html",list=graphs)
        elif request.form["submit"]=="find_property":
             title=request.form["title"]
             print("title test 1",title)
             query="""
        MATCH(a:Event{title:$title})
        return a.title as title ,a.date as date, a.lieu as lieu
        """
             parameter={"title":title}
             results=session.run(query,parameter)
             ParamsOfGivenEventList=[]
             for result in results:
                ParamsEvents={}
                title=result["title"]
                date=result["date"]
                lieu=result["lieu"]
                ParamsEvents.update({"Title":title,"Date":date, "Lieu":lieu})
                ParamsOfGivenEventList.append(ParamsEvents)

             if(len(ParamsOfGivenEventList)>0):
              return render_template("friends.html",list=ParamsOfGivenEventList,title=title)
             else:
                return("no Events available")
    else:
        return render_template("index.html")

if __name__=='__main__':
    app.run(port=5000)
